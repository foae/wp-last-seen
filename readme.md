[![Code Climate](https://codeclimate.com/github/foae/wp-last-seen/badges/gpa.svg)](https://codeclimate.com/github/foae/wp-last-seen) 
[![Codacy Badge](https://api.codacy.com/project/badge/80620a11048e429489e1fecae79e74c3)](https://www.codacy.com/app/eastercow/wp-last-seen) 
[![SensioLabsInsight](https://insight.sensiolabs.com/projects/1425665a-b823-4b88-beb7-2d66474cca19/mini.png)](https://insight.sensiolabs.com/projects/1425665a-b823-4b88-beb7-2d66474cca19) 
[![Gitter](https://badges.gitter.im/Join%20Chat.svg)](https://gitter.im/foae/wp-last-seen?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=body_badge) 

# WP Last Seen #

WP Last Seen displays a widget in front end with information regarding when was the last time a member has been seen online.

**[WP Last Seen on WordPress.com](https://wordpress.org/plugins/wp-last-seen/ "WP Last Seen on WordPress.com")**

## Description ##

WP Last Seen is a nice lightweight widget for your WordPress site which displays time information regarding your members (registered users) and when was the last time they were active on your site.

The status is shown as

* icon status
* username
* last active timestamp (shown in days, hours, minutes, seconds)

## Installation ##

1. In your WordPress backend (admin menu), go to `Plugins -> Add New` and search for `WP Last Seen`
2. Click `Install`, then `Activate`
3. In your `Appearance` menu, go to `Widgets` and place `WP Last Seen Widget` in your desired location. The title of the widget is optional.

That's it!
